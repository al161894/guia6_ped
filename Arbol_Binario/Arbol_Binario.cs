﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Arbol_Binario
{
    class Arbol_Binario
    {
        public Nodo_Arbol Raiz;
        public Nodo_Arbol aux;

        public Arbol_Binario()
        {
            aux = new Nodo_Arbol();
        }

        public Arbol_Binario(Nodo_Arbol nueva_raiz)
        {
            Raiz = nueva_raiz;
        }

        //funcion para agregar un nuevo nodo(valor) al arbol binario
        public void Insertar(int x)
        {
            if(Raiz == null)
            {
                Raiz = new Nodo_Arbol(x, null, null, null);
                Raiz.nivel = 0;
            }
            else
            {
                Raiz = Raiz.Insertar(x, Raiz, Raiz.nivel);
            }
        }

        public void Preorden(Nodo_Arbol raiz)
        {
            MessageBox.Show(raiz.info.ToString());
            if (raiz.Izquierdo != null)
            {
                Preorden(raiz.Izquierdo);                
            }                

            if (raiz.Derecho != null)
            {
                Preorden(raiz.Derecho);                
            }                
        }

        public void Enorden(Nodo_Arbol raiz)
        {
            if(raiz.Izquierdo != null)
            {
                Enorden(raiz.Izquierdo);
            }
            MessageBox.Show(raiz.info.ToString());
            if(raiz.Derecho != null)
            {
                Enorden(raiz.Derecho);
            }
        }

        public void PostOrden(Nodo_Arbol raiz)
        {
            if(raiz.Izquierdo != null)
            {
                PostOrden(raiz.Izquierdo);
            }
            if(raiz.Derecho != null)
            {
                PostOrden(raiz.Derecho);
            }
            MessageBox.Show(raiz.info.ToString());
        }

        //funcion para eliminar un nodo(valor) del arbol binario
        public void Eliminar(int x)
        {
            if (Raiz == null)
                Raiz = new Nodo_Arbol(x, null, null, null);
            else
                Raiz.Eliminar(x, ref Raiz);
        }

        public void Buscar(int x)
        {
            if(Raiz != null)
            {
                Raiz.buscar(x, Raiz);
            }
        }

        /**Funciones para dibujar el arbol binario en el formulario **/
        //funcion para dibujar arbol binario
        public void DibujarArbol(Graphics grafo, Font fuente, Brush Relleno, Brush RellenoFuente, Pen Lapiz, Brush encuentro)
        {
            int x = 400;//posiciones de la raiz del arbol
            int y = 75;
            if (Raiz == null)
                return;
            Raiz.PosicionNodo(ref x, y);//posicion de cada nodo
            Raiz.DibujarRamas(grafo, Lapiz);//Dibuja los enlces entre nodos
            //dibuja todos los nodos
            Raiz.DibujarNodo(grafo, fuente, Relleno, RellenoFuente, Lapiz, encuentro);
        }

        public int x1 = 400;
        //posiciones iniciales de la raiz dela rbol
        public int y2 = 75;
        //funcion para colorear los nodos
        public void colorear(Graphics grafo, Font fuente, Brush Relleno, Brush RellenoFuente, Pen Lapiz, Nodo_Arbol Raiz, bool post, bool inor, bool preor)
        {
            Brush entorno = Brushes.Red;
            if(inor == true)
            {
                if(Raiz != null)
                {
                    colorear(grafo, fuente, Relleno, RellenoFuente, Lapiz, Raiz.Derecho, post, inor, preor);
                    Thread.Sleep(1000);

                    //pausar la ejecucion 1000 ms
                    Raiz.colorear(grafo, fuente, Relleno, RellenoFuente, Lapiz);
                    colorear(grafo, fuente, Relleno, RellenoFuente, Lapiz, Raiz.Derecho, post, inor, preor);
                }
            }
            else
            {
                if(preor == true)
                {
                    if(Raiz != null)
                    {
                        Raiz.colorear(grafo, fuente, entorno, RellenoFuente, Lapiz);
                        Thread.Sleep(1000);
                        //pausar la ejecucion 1000ms
                        Raiz.colorear(grafo, fuente, Relleno, RellenoFuente, Lapiz);
                        colorear(grafo, fuente, Relleno, RellenoFuente, Lapiz, Raiz.Izquierdo, post, inor, preor);
                        colorear(grafo, fuente, Relleno, RellenoFuente, Lapiz, Raiz.Derecho, post, inor, preor);
                    }
                }
                else
                {
                    if(post == true)
                    {
                        if(Raiz != null)
                        {
                            colorear(grafo, fuente, Relleno, RellenoFuente, Lapiz, Raiz.Izquierdo, post, inor, preor);
                            colorear(grafo, fuente, Relleno, RellenoFuente, Lapiz, Raiz.Derecho, post, inor, preor);
                            Raiz.colorear(grafo, fuente, entorno, RellenoFuente, Lapiz);
                            Thread.Sleep(1000);//pausar la ejecucion 1000 ms
                            Raiz.colorear(grafo, fuente, Relleno, RellenoFuente, Lapiz);
                        }
                    }
                }
            }
        }

    }
}
