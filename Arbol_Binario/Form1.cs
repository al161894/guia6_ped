﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Arbol_Binario
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //declaracion de variables a utilizar
        int Dato = 0;
        int cont = 0;
        Arbol_Binario mi_Arbol = new Arbol_Binario(null);//creacion del objeto arbol
        Graphics g;//definicion del objeto grafico

        //evento del formulario que permitirá dibujar el arbol binario
        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.Clear(this.BackColor);
            e.Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            g = e.Graphics;
            mi_Arbol.DibujarArbol(g, this.Font, Brushes.Blue, Brushes.White, Pens.Black, Brushes.White);
        }

        /**Evento que permitirá insertar un nodo al arbol **/
        private void button1_Click(object sender, EventArgs e)
        {
            if(txtDato.Text == "")
            {
                MessageBox.Show("Debe ingresar un valor");
            }
            else
            {
                Dato = int.Parse(txtDato.Text);
                if (Dato <= 0 || Dato >= 100)
                    MessageBox.Show("Solo recibe valores desde 1 hasta 99", "Error de ingreso");
                else
                {
                    mi_Arbol.Insertar(Dato);
                    txtDato.Clear();
                    txtDato.Focus();
                    cont++;
                    Refresh();
                    Refresh();
                }

            }
        }

        /*Evento que permitira eliminar un nodo del arbol */
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if(txtEliminar.Text == "")
            {
                MessageBox.Show("Debe ingresar el valor a eliminar");
            }
            else
            {
                Dato = Convert.ToInt32(txtEliminar.Text);
                if(Dato <= 0 || Dato >= 100)
                {
                    MessageBox.Show("Solo se admiten valores entre 1 y 99", "Error de ingreso");
                }
                else
                {
                    mi_Arbol.Eliminar(Dato);
                    txtEliminar.Clear();
                    txtEliminar.Focus();
                    cont--;
                    Refresh();
                    Refresh();
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if(txtBuscar.Text == "")
            {
                MessageBox.Show("Debe ingresar el valor a buscar");
            }
            else
            {
                Dato = Convert.ToInt32(txtBuscar.Text);
                if(Dato <= 0 || Dato >= 100)
                {
                    MessageBox.Show("Solo se admiten valroes entre 1 y 99", "Error de ingreso");
                }
                else
                {
                    mi_Arbol.Buscar(Dato);
                    txtBuscar.Clear();
                    txtBuscar.Focus();
                    Refresh();
                    Refresh();
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            mi_Arbol.Preorden(mi_Arbol.Raiz);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            mi_Arbol.Enorden(mi_Arbol.Raiz);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            mi_Arbol.PostOrden(mi_Arbol.Raiz);
        }
    }
}
