﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading; //libreria para manejo de hilos
using System.Windows.Forms;


//clase creada para definir el elemento nodo del arbol
namespace Arbol_Binario
{
    class Nodo_Arbol
    {
        public int info; //dato a almacenar en el nodo
        public Nodo_Arbol Izquierdo;//nodo izquierdo del arbol
        public Nodo_Arbol Derecho;//nodo derecho del arbol
        public Nodo_Arbol Padre;//nodo raiz del arbol
        public int altura;
        public int nivel;
        public Rectangle nodo;//para dibujar el nodo del arbol

        //variable que definen el tamaño de los circulos que representan los nodos del arbol
        private const int Radio = 30;
        //variable para el manejo de distancia horizontal
        private const int DistanciaH = 80;
        //variable para el manejo de distancia vertical
        private const int DistanciaV = 10;
        //variable para manejar posicion eje X
        private int CoordenadaX;
        //variable para manejar posicion eje y
        private int CoordenadaY;
        Graphics col;
        private Arbol_Binario arbol;//declarando un objeto de tipo arbol

        public Nodo_Arbol()//constructor por defecto
        {
        }

        //constructor por defecto para el objeto de tipo arbol
        public Arbol_Binario Arbol
        {
            get { return arbol; }
            set { arbol = value; }
        }

        //constructor con parametros
        public Nodo_Arbol(int nueva_info, Nodo_Arbol izquierdo, Nodo_Arbol derecho, Nodo_Arbol padre)
        {
            info = nueva_info;
            Izquierdo = izquierdo;
            Derecho = derecho;
            Padre = padre;
            altura = 0;
        }

        //funcion para insertar un nodo en el arbol
        public Nodo_Arbol Insertar(int x, Nodo_Arbol t, int Level)
        {
            //en esta funcion t representa un nodo del arbol(raiz)
            //si el arbol no tiene ningun nodo
            if (t == null)
            {
                t = new Nodo_Arbol(x, null, null, null);
                t.nivel = Level;
            }
            else if (x < t.info)//si el valor a insertar es menor que la raiz
            {
                Level++;
                t.Izquierdo = Insertar(x, t.Izquierdo, Level);
            } else if (x > t.info) {//si el valor a insertar es mayor que la raiz
                Level++;
                t.Derecho = Insertar(x, t.Derecho, Level);
            }
            else
            {
                MessageBox.Show("Dato existente en el Arbol", "Error de ingreso");
            }
            return t;
        }

        //funcion para eliminar un nodo de un arbol binario
        public void Eliminar(int x, ref Nodo_Arbol t)
        {
            if(t != null)//si la raiz es distinta de null
            {
                if(x < t.info)//si el valor a eliminar es menor que la raiz
                {
                    Eliminar(x, ref t.Izquierdo);
                }
                else
                {
                    if(x > t.info)//si el valor a eliminar es mayor que la raiz
                    {
                        Eliminar(x, ref t.Derecho);
                    }
                    else
                    {
                        Nodo_Arbol NodoEliminar = t;//se ubica el nodo a eliminar 
                        //se verifica si tiene hijo derecho
                        if(NodoEliminar.Derecho == null)
                        {
                            t = NodoEliminar.Izquierdo;
                        }
                        else
                        {
                            //se verifica si tiene hijo izquierdo
                            if (NodoEliminar.Izquierdo == null)
                            {
                                t = NodoEliminar.Derecho;
                            }
                            else
                            {
                                if(Alturas(t.Izquierdo) - Alturas(t.Derecho) > 0)
                                    //para verificar que hijo pasa a ser nueva raiz del subarbol
                                {
                                    Nodo_Arbol AuxiliarNodo = null;
                                    Nodo_Arbol Auxiliar = t.Izquierdo;
                                    bool bandera = false;

                                    while (Auxiliar.Derecho != null)
                                    {
                                        AuxiliarNodo = Auxiliar;
                                        Auxiliar = Auxiliar.Derecho;//se moviliza el auxiliar
                                        bandera = true;
                                    }
                                    //se crea nodo temporal
                                    t.info = Auxiliar.info;
                                    NodoEliminar = Auxiliar;

                                    if(bandera == true)
                                    {
                                        AuxiliarNodo.Derecho = Auxiliar.Izquierdo;
                                    }
                                    else
                                    {
                                        t.Izquierdo = Auxiliar.Izquierdo;
                                    }
                                }
                                else
                                {
                                    if(Alturas(t.Derecho) - Alturas(t.Izquierdo) > 0)
                                    {
                                        Nodo_Arbol AuxiliarNodo = null;
                                        Nodo_Arbol Auxiliar = t.Derecho;
                                        bool bandera = false;

                                        while(Auxiliar.Izquierdo != null)
                                        {
                                            AuxiliarNodo = Auxiliar;
                                            Auxiliar = Auxiliar.Izquierdo;
                                            bandera = true;
                                        }
                                        t.info = Auxiliar.info;
                                        NodoEliminar = Auxiliar;
                                        if(bandera == true)
                                        {
                                            AuxiliarNodo.Izquierdo = Auxiliar.Derecho;
                                        }
                                        else
                                        {
                                            t.Derecho = Auxiliar.Derecho;
                                        }
                                    }
                                    else
                                    {
                                        if(Alturas(t.Derecho) - Alturas(t.Izquierdo) == 0)
                                        {
                                            Nodo_Arbol AuxiliarNodo = null;
                                            Nodo_Arbol Auxiliar = t.Izquierdo;
                                            bool bandera = false;

                                            while(Auxiliar.Derecho != null)
                                            {
                                                AuxiliarNodo = Auxiliar;
                                                Auxiliar = Auxiliar.Derecho;
                                                bandera = true;
                                            }
                                            t.info = Auxiliar.info;
                                            NodoEliminar = Auxiliar;

                                            if(bandera == true)
                                            {
                                                AuxiliarNodo.Derecho = Auxiliar.Izquierdo;
                                            }
                                            else
                                            {
                                                t.Izquierdo = Auxiliar.Izquierdo;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Nodo NO existe en el arbol", "Error de eliminación");
            }
        }//final de la funcion eliminar


        //funcion buscar un nodo
        public void buscar(int x, Nodo_Arbol t)
        {
            if(t != null)
            {
                if(x == t.info)
                {
                    MessageBox.Show("Nodo encontrado en la posicion X: " + t.CoordenadaX + "Y: " + t.CoordenadaY);
                    encontrado(t);
                }
                else
                {
                    if(x < t.info)//busqueda en el subarbol izquierdo
                    {
                        buscar(x, t.Izquierdo);
                    }
                    else
                    {
                        if(x > t.info)//busqueda en el subarbol derecho
                        {
                            buscar(x, t.Derecho);
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Nodo NO encontrado", "Error de busqueda");
            }
        }

        //funcion posicion nodo (donde se ha creado dibujo del nodo)
        public void PosicionNodo(ref int xmin, int ymin)
        {
            int aux1, aux2;
            CoordenadaY = (int)(ymin + Radio / 2);

            //obtiene la posicion del subarbol izquierdo
            if(Izquierdo != null)
            {
                Izquierdo.PosicionNodo(ref xmin, ymin + Radio + DistanciaV);
            }

            if((Izquierdo != null) && (Derecho != null))
            {
                xmin += DistanciaH;
            }
            //si existe nodo derecho y el nodo izquierdo deja un espacion entre ellos
            if(Derecho != null)
            {
                Derecho.PosicionNodo(ref xmin, ymin + Radio + DistanciaV);
            }

            if(Izquierdo != null && Derecho != null)
            {
                CoordenadaX = (int)((Izquierdo.CoordenadaX + Derecho.CoordenadaX) / 2);
            }
            else
            {
                if(Izquierdo != null)
                {
                    aux1 = Izquierdo.CoordenadaX;
                    Izquierdo.CoordenadaX = CoordenadaX - 80;
                    CoordenadaX = aux1;
                }
                else
                {
                    if(Derecho != null)
                    {
                        aux2 = Derecho.CoordenadaX;
                        //no hay nodo izquierdo, centrar en nodo derecho
                        Derecho.CoordenadaX = CoordenadaX + 80;
                        CoordenadaX = aux2;
                    }
                    else
                    {
                        CoordenadaX = (int)(xmin + Radio / 2);xmin += Radio;
                    }
                }
            }
        }

        //funcion para dibujar las ramas  de los nodos izquierdo y derecho
        public void DibujarRamas(Graphics grafo, Pen Lapiz)
        {
            if(Izquierdo != null)
                //dibujará rama izquierda
            {
                grafo.DrawLine(Lapiz, CoordenadaX, CoordenadaY, Izquierdo.CoordenadaX, Izquierdo.CoordenadaY);
                Izquierdo.DibujarRamas(grafo, Lapiz);
            }
            if(Derecho != null)
                //dibujara rama derecha
            {
                grafo.DrawLine(Lapiz, CoordenadaX, CoordenadaY, Derecho.CoordenadaX, Derecho.CoordenadaY);
                Derecho.DibujarRamas(grafo, Lapiz);
            }
        }

        //funcion para dibujar el nodo en la posicion especifica
        public void DibujarNodo(Graphics grafo, Font fuente, Brush Relleno, Brush RellenoFuente, Pen Lapiz, Brush encuentro)
        {
            col = grafo;
            //dibuja el contorno del nodo
            Rectangle rect = new Rectangle((int)(CoordenadaX - Radio / 2), (int)(CoordenadaY - Radio / 2), Radio, Radio);
            Rectangle prueba = new Rectangle((int)(CoordenadaX - Radio / 2), (int)(CoordenadaY - Radio / 2), Radio, Radio);
            grafo.FillEllipse(encuentro, rect);
            grafo.FillEllipse(Relleno, rect);
            grafo.DrawEllipse(Lapiz, rect);
            //para dibujar el nombre del nodo, es decir el contenido
            StringFormat formato = new StringFormat();
            formato.Alignment = StringAlignment.Center;
            formato.LineAlignment = StringAlignment.Center;
            grafo.DrawString(info.ToString(), fuente, RellenoFuente, CoordenadaX, CoordenadaY, formato);

            //dibuja los nodos hijos derecho e izquierdo
            if(Izquierdo != null)
            {
                Izquierdo.DibujarNodo(grafo, fuente, Relleno, RellenoFuente, Lapiz, encuentro);
            }
            if(Derecho != null)
            {
                Derecho.DibujarNodo(grafo, fuente, Relleno, RellenoFuente, Lapiz, encuentro);
            }
        }

        public void colorear(Graphics grafo, Font fuente, Brush Relleno, Brush RellenoFuerte, Pen Lapiz)
        {
            //dibuja el contorno del nodo
            Rectangle rect = new Rectangle((int)(CoordenadaX - Radio / 2), (int)(CoordenadaY - Radio / 2), Radio, Radio);
            Rectangle prueba = new Rectangle((int)(CoordenadaX - Radio / 2), (int)(CoordenadaY - Radio / 2), Radio, Radio);
            grafo.FillEllipse(Relleno, rect);
            grafo.DrawEllipse(Lapiz, rect);
            //dibuja el nombre
            StringFormat formato = new StringFormat();
            formato.Alignment = StringAlignment.Center;
            formato.LineAlignment = StringAlignment.Center;
            grafo.DrawString(info.ToString(), fuente, RellenoFuerte, CoordenadaX, CoordenadaY, formato);
        }

        //verificar altura del arbol
        private static int Alturas(Nodo_Arbol t)
        {
            return t == null ? -1 : t.altura;
        }

        public void encontrado(Nodo_Arbol t)
        {
            Rectangle rec = new Rectangle(t.CoordenadaX, t.CoordenadaY, 40, 40);
        }

    }
}
